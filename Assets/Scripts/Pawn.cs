﻿using GameAnalyticsSDK;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{
    public int CurrentLevel
    {
        get { return currentLevel; }
        set { currentLevel = value; }
    }
    public int LevelsCount => _levelPrefabs.Count;
    public bool Failed => failed;
    public bool Success => success;
    public bool Win => win;
    public bool Paused => paused;

    [SerializeField] private float force = 100f;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private List<GameObject> _levelPrefabs;

    private Rigidbody2D rb;
    private GameObject level;
    private Vector2 direction;
    private int currentLevel;
    private bool failed;
    private bool success;
    private bool paused;
    private bool win;

    void Awake()
    {
        currentLevel = 1;
        level = Instantiate(_levelPrefabs[0]);
        transform.position = _spawnPoint.position;

        SwipeDetector.OnSwipe += PawnMovement;
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Exit") && !paused)
        {
            if (currentLevel == _levelPrefabs.Count) win = true;
            success = paused = true;

            GameAnalytics.NewDesignEvent($"Level Complete: {currentLevel}", currentLevel);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && !paused)
        {
            failed = paused = true;
        }
    }

    public void StartLevel(bool next)
    {
        Destroy(level);
        success = failed = paused = false;
        transform.position = _spawnPoint.position;
        rb.velocity = new Vector2(0, 0);

        if (next)
        {
            level = Instantiate(_levelPrefabs[currentLevel]);
            currentLevel++;
        }
        else level = Instantiate(_levelPrefabs[currentLevel - 1]);
    }

    private void PawnMovement(SwipeData data)
    {
        if (!paused)
        {
            Vector2 start = Camera.main.ScreenToWorldPoint(new Vector3(data.StartPosition.x, data.StartPosition.y));
            Vector2 end = Camera.main.ScreenToWorldPoint(new Vector3(data.EndPosition.x, data.EndPosition.y));
            direction = start - end;

            rb.AddRelativeForce(direction * force, ForceMode2D.Force);
        }
    }
}
