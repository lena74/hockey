﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
    [SerializeField] private Pawn _pawn;
    [Header("Text")]
    [SerializeField] private TextMeshProUGUI _levelText;
    [Header("Panels")]
    [SerializeField] private GameObject _failPanel;
    [SerializeField] private GameObject _successPanel;
    [SerializeField] private GameObject _winPanel;
    [Header("Buttons")]
    [SerializeField] private Button _nextButton;
    [SerializeField] private Button _restartButton;

    void Awake()
    {
        _failPanel.SetActive(false);
        _successPanel.SetActive(false);
        _winPanel.SetActive(false);

        _restartButton.onClick.AddListener(() =>
        {
            _pawn.StartLevel(false); 
            _successPanel.SetActive(false);
            _failPanel.SetActive(false);
        });

        _nextButton.onClick.AddListener(() =>
        {
            _pawn.StartLevel(true);
            _successPanel.SetActive(false); 
            _failPanel.SetActive(false);
        });
    }

    void Update()
    {
        _levelText.text = "Level " + _pawn.CurrentLevel.ToString();

        if (_pawn.Paused)
        {
            _failPanel.SetActive(_pawn.Failed);
            _successPanel.SetActive(_pawn.Success);
            _winPanel.SetActive(_pawn.Win);
        }
    }
}
